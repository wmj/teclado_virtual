"use strict";

class VirtualKeyboard {

    static insertText(element, text) {
        const oldText = element.value;
        const previous = oldText.slice(0, element.selectionStart);
        const posterior = oldText.slice(element.selectionEnd);
        const newText = `${previous}${text}${posterior}`;

        // set new text
        element.value = newText;
        // update selection
        element.selectionStart = previous.length + text.length;
        element.selectionEnd = element.selectionStart;
        // set focus
        element.focus();
    }

    static register(element){
        element.addEventListener("focus", (ev) => {
            this.updateFocus(element);
        });
        this.updateFocus(element);
        return element;
    }

    static updateFocus(element) {
        this.LAST_FOCUS = element;
    }

    static onKeyPressed(ev){
        const target = ev.target;
        if(!target.classList.contains("vkey")) return;
        ev.preventDefault();
        const input = this.LAST_FOCUS;
        if(!input) return;
        this.insertText(input, target.innerText);
    }

    static render(parentElement) {
        const insertKey = (parent, key) => {
            const el = document.createElement("a");
            el.classList.add("vkey");
            el.innerText = key;
            el.tabIndex = 0;
            parent.appendChild(el);
        };
        const frag = document.createDocumentFragment();
        const buttons = this.KEYS.forEach((x) => {
            const _isArray = Array.isArray(x);
            if(_isArray && (x.length > 1)){
                let group = document.createElement("div");
                group.classList.add("vkey-group");
                let gFrag = document.createDocumentFragment();
                x.forEach((key) => {
                    insertKey(gFrag, key);
                });
                group.appendChild(gFrag);
                frag.appendChild(group);
            }else{
                if(_isArray){
                    x = x[0];
                }
                insertKey(frag, x);
            }
        });
        parentElement.innerHTML = "";
        parentElement.appendChild(frag);
        parentElement.addEventListener("click", this.onKeyPressed.bind(this));
        parentElement.addEventListener("keydown", (ev) => {
            if(ev.key == "Enter"){
                this.onKeyPressed(ev);
            }
        });
    }
};
VirtualKeyboard.LAST_FOCUS = null;
VirtualKeyboard.KEYS = [
    ['á', 'à', 'â', 'ã'],
    ['é', 'è', 'ê', 'ẽ'],
    ['í', 'ì', 'î', 'ĩ'],
    'ñ',
    ['l'],
    ['ó', 'ò', 'ô', 'õ'],
    ['ú', 'ù', 'û', 'ũ'],
];

(function(document){

    document.addEventListener('DOMContentLoaded', (event) => {
        const inputs = document.getElementsByClassName("with-keyboard");
        Array.from(inputs).forEach((el) => VirtualKeyboard.register(el));
        const el = document.querySelector(".teclado");
        VirtualKeyboard.render(el);
    });

})(document);
