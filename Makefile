LESS := node_modules/less/bin/lessc

all: build/teclado.css

build/%.css: src/%.less
	$(LESS) $< > $@
